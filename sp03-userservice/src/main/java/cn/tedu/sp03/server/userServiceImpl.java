package cn.tedu.sp03.server;

import cn.tedu.sp01.pojo.User;
import cn.tedu.sp01.service.UserService;
import cn.tedu.web.util.JsonUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class userServiceImpl implements UserService {

    @Value("${sp.user-service.users}")
    private String userJson;

    @Override
    public User getUser(Integer id) {
        log.info("获取用户,id="+id);
        //userJson --> List<User>  com.fasterxml.jackson.core.type
        //TypeReference 只是利用继承语法写要转换的类型
        List<User> list =JsonUtil.from(
                userJson,new TypeReference<List<User>>(){});
        for (User u: list) {
            if (u.getId().equals(id)){
                return u;
            }
        }
        return new User(id,"用户名"+id,"密码"+id);

    }

    @Override
    public void addScore(Integer id, Integer score) {
        log.info("增加用户积分,id="+id+",score="+score);
    }
}
