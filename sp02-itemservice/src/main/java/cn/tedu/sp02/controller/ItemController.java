package cn.tedu.sp02.controller;

import cn.tedu.sp01.pojo.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@Slf4j
@RestController
public class ItemController {
    @Autowired
    private ItemService itemService;
    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String order) throws InterruptedException {
        List<Item> items = itemService.getItems(order);
        JsonResult r= JsonResult.ok().data(items);
        //随机的延迟代码
        if(Math.random()<0.9){
            //随机延迟时长 0-5秒
            int t= new Random().nextInt(5000);
            log.info("随机延迟: "+t);
            Thread.sleep(t);
        }
        return  r;
    }



    //减少商品库存
    /*
    * @RequestBody 接受客户端接受的参数
    * 完整的接受http协议体数据  再转成java对象
    * */
    @PostMapping("/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items){
        itemService.decreaseNumber(items);
        return JsonResult.ok().msg("减少商品库存成功");
    }
}
