package cn.tedu.sp06.filter;

import com.netflix.zuul.ZuulFilter;
import com.netflix.zuul.context.RequestContext;
import com.netflix.zuul.exception.ZuulException;
import org.springframework.cloud.netflix.zuul.filters.support.FilterConstants;
import org.springframework.stereotype.Component;

@Component
public class AccessFilter extends ZuulFilter {
    //判断过滤器类型  pre(前置),routing,post,error
    @Override
    public String filterType() {
        return "pre";
    }
    //位置序号
    @Override
    public int filterOrder() {
        return 6;
    }
    //判断针对当前请求 是否执行过滤代码
    @Override
    public boolean shouldFilter() {
        /*
        调用商品判断权限
        调用用户和订单,不检查权限
         */

        //获得调用的服务id
        RequestContext ctx=RequestContext.getCurrentContext();
        String ServiceId= (String) ctx.get(FilterConstants.SERVICE_ID_KEY);

        return "Item-service".equals(ServiceId);
    }
    //过滤代码
    @Override
    public Object run() throws ZuulException {
        return null;
    }
}
