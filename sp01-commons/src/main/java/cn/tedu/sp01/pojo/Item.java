package cn.tedu.sp01.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor//无参构造方法
@AllArgsConstructor//全参构造方法
public class Item{
    private Integer id;
    private String name;    //商品名称
    private Integer number; //商品数量



}
